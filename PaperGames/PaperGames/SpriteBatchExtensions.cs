﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PaperGames
{
	public static class SpriteBatchExtensions
	{
		public static Texture2D Point { get; private set; }
		private static GraphicsDevice _theGraphicDevice;

		public static void Initialize( GraphicsDevice theGraphicDevice )
		{
			_theGraphicDevice = theGraphicDevice;

			Point = new Texture2D( _theGraphicDevice, 1, 1, false, SurfaceFormat.Color );
			Point.SetData( new[] { Color.White } );
		}

		public static void DrawLine( this SpriteBatch batch, Vector2 point1, Vector2 point2, Color color, float width = 2f )
		{
			float angle = ( float ) Math.Atan2( point2.Y - point1.Y, point2.X - point1.X );
			float length = Vector2.Distance( point1, point2 );

			batch.Draw( Point, point1, null, color, angle, Vector2.Zero, new Vector2( length, width ), SpriteEffects.None, 0 );
		}
	}

}
