﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using PaperGames.MiniGames;


namespace PaperGames.Windows
{
	/// <summary>
	/// Interaction logic for BiscuitGameWindow.xaml
	/// </summary>
	public partial class BiscuitGameWindow
	{
		private int _playersCount;
		private bool _isGameRunning;
		private static Random _baseRandom;

		public BiscuitGameWindow()
		{
			InitializeComponent();

			_baseRandom = new Random();
			BiscuitPlayersCountSlider.Minimum = 1;
		}

		private void BiscuitPlayersSliderValueChanged( object sender, RoutedPropertyChangedEventArgs<double> e )
		{
			var restOfValue = Math.Abs( e.NewValue - e.OldValue );
			while ( restOfValue > 0 )
			{
				if ( e.NewValue > e.OldValue )
				{
					AddPlayerDetailsSlot();
				}
				else
				{
					RemovePlayerDetailsSlot();
				}
				restOfValue -= 1;
			}
		}

		private void AddPlayerDetailsSlot()
		{
			int playerId = _playersCount + 1;

			FontFamily fontFamily = new FontFamily( "My very own handwriting." );

			Grid detailsGrid = new Grid { Margin = new Thickness( 5 ) };
			detailsGrid.ColumnDefinitions.Add( new ColumnDefinition { Width = new GridLength( 30, GridUnitType.Pixel ) } );
			detailsGrid.ColumnDefinitions.Add( new ColumnDefinition { Width = new GridLength( 1, GridUnitType.Star ) } );

			Label playerIdLabel = new Label { Content = playerId, FontFamily = fontFamily, FontSize = 24 };
			Grid.SetColumn( playerIdLabel, 0 );

			TextBox playerNameTextBox = new TextBox { FontFamily = fontFamily, FontSize = 24, CharacterCasing = CharacterCasing.Upper, Background = null };
			Grid.SetColumn( playerNameTextBox, 1 );

			detailsGrid.Children.Add( playerIdLabel );
			detailsGrid.Children.Add( playerNameTextBox );

			PlayersDetailsStackPanel.Children.Add( detailsGrid );
			_playersCount++;
		}

		private void RemovePlayerDetailsSlot()
		{
			if ( _playersCount > 0 )
			{
				PlayersDetailsStackPanel.Children.RemoveAt( _playersCount - 1 );
				_playersCount--;
			}
		}
		private Microsoft.Xna.Framework.Color RandomColor()
		{
			byte r = ( byte ) _baseRandom.Next( 0, 255 );
			byte g = ( byte ) _baseRandom.Next( 0, 255 );
			byte b = ( byte ) _baseRandom.Next( 0, 255 );

			return new Microsoft.Xna.Framework.Color( r, g, b );
		}
		private void PlayGame( object sender, System.Windows.Input.MouseButtonEventArgs e )
		{
			if ( _isGameRunning )
			{
				MessageBox.Show( "A game is already started!" );
				return;
			}
			if ( PlayersDetailsStackPanel.Children.Count < 2 && BiscuitComputerCountSlider.Value == 0 )
			{
				MessageBox.Show( "You must choose at least two kind of player to play(Player, Computer)!" );
				return;
			}
			_isGameRunning = true;

			int boardSize = ( int ) BiscuitSizeSlider.Value;
			List<Player> players = new List<Player>( _playersCount );
			foreach ( var child in PlayersDetailsStackPanel.Children )
			{
				Grid childGrid = ( Grid ) child;
				TextBox playerNameTextBox = childGrid.Children.OfType<TextBox>().First();

				Player player = new Player
				{
					Name = playerNameTextBox.Text,
					Color = RandomColor(),
					CellPattern = _baseRandom.Next( 4 ),
					isComputer = false
				};

				players.Add( player );
			}
			for ( int i = 1; i <= BiscuitComputerCountSlider.Value; i++ )
			{
				Player player = new Player
				{
					Name = "Computer" + i,
					Color = RandomColor(),
					CellPattern = _baseRandom.Next( 4 ),
					isComputer = true
				};
				players.Add( player );
			}

			Thread thread = new Thread( () =>
			{
				using ( MainGame mainGame = new MainGame( MiniGameType.Biscuit, players, boardSize ) )
				{
					mainGame.Exiting += ( s, ev ) => { _isGameRunning = false; };
					mainGame.Run();
				}
			} );
			thread.Start();
			thread.Join();
		}

		private void Slider_ValueChanged( object sender, RoutedPropertyChangedEventArgs<double> e )
		{

		}
	}
}
