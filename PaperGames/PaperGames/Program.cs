using System;
using System.Windows;
using PaperGames.Windows;

namespace PaperGames
{
#if WINDOWS || XBOX
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main( string[] args )
		{
			MainWindow window = new MainWindow();
			AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
			Application application = new Application();
			application.Run( window );
		}

		private static void CurrentDomainOnUnhandledException( object sender, UnhandledExceptionEventArgs unhandledExceptionEventArgs )
		{
			if ( unhandledExceptionEventArgs.ExceptionObject != null )
			{
				MessageBox.Show( "Error!" + unhandledExceptionEventArgs.ExceptionObject );
			}
		}
	}
#endif
}

