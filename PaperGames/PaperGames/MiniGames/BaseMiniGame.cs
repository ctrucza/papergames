﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace PaperGames.MiniGames
{
	public abstract class BaseMiniGame
	{
		public abstract void InitGame( List<Player> players, int boardSize );
		public abstract void LoadContent( ContentManager contentManager );
		public abstract void Update( GameTime gameTime );
		public abstract void Draw( SpriteBatch spriteBatch, GameTime gameTime );

		public List<Player> Players { get; set; }

		public bool IsFinished { get; protected set; }

		protected BaseMiniGame()
		{
			Players = new List<Player>();
		}
	}
}
