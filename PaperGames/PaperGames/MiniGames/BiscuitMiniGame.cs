﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PaperGames.MiniGames
{
	public class BiscuitMiniGame : BaseMiniGame
	{
		private const int CellSize = 40;
		private const int CircleRadius = 15;

		private const int LineScoreIncrement = 5;
		private const int CellScoreIncrement = 10;

		private static Random randomSides;

		private readonly GraphicsDeviceManager _graphicsDeviceManager;

		private Vector2 _gameOffset;

		private SpriteFont _font;
		private int _currentPlayerIndex;
		private Player CurrentPlayer { get { return Players[_currentPlayerIndex]; } }

		private BiscuitCell[,] _mainBiscuit;
		private List<BiscuitCell> _uncompletedCells;

		private int _size;
		private Texture2D _verticalLineTexture;
		private Texture2D _horizontalLineTexture;
		private Texture2D _verticalBackgroundLineTexture;
		private Texture2D _horizontalBackgroundLineTexture;

		private Texture2D[] _cellFillsTextures;
		private const int CellFillsTexturesCount = 4;

		private Vector2 _lineStart = Vector2.Zero;
		private Vector2 _lineEnd = Vector2.Zero;

		private MouseState _previousMouseState;
		private MouseState _currentMouseState;

		public BiscuitMiniGame( GraphicsDeviceManager graphics )
		{
			_graphicsDeviceManager = graphics;
		}

		public override void InitGame( List<Player> players, int boardSize )
		{
			_currentPlayerIndex = 0;
			Players = players;
			CreateBiscuit( boardSize );
			randomSides = new Random();

			const int margin = 50;
			const int currentPlayerTextSize = 50;

			_graphicsDeviceManager.PreferredBackBufferWidth = Math.Max( 1200, boardSize * CellSize + margin * 3 );
			_graphicsDeviceManager.PreferredBackBufferHeight = boardSize * CellSize + margin * 2 + currentPlayerTextSize;
			_graphicsDeviceManager.ApplyChanges();

			_gameOffset = new Vector2( _graphicsDeviceManager.PreferredBackBufferWidth / 2 - boardSize / 2 * CellSize - margin, 50 );
		}

		public override void LoadContent( ContentManager contentManager )
		{
			_font = contentManager.Load<SpriteFont>( "MainFont" );
			_verticalLineTexture = contentManager.Load<Texture2D>( "line_vertical" );
			_horizontalLineTexture = contentManager.Load<Texture2D>( "line_horizontal" );
			_verticalBackgroundLineTexture = contentManager.Load<Texture2D>( "line_verticalBackground" );
			_horizontalBackgroundLineTexture = contentManager.Load<Texture2D>( "line_horizontalBackground" );


			_cellFillsTextures = new Texture2D[CellFillsTexturesCount];
			for ( int i = 0; i < CellFillsTexturesCount; i++ )
			{
				_cellFillsTextures[i] = contentManager.Load<Texture2D>( "cellFills/cellFill" + i );
			}
		}

		public override void Update( GameTime gameTime )
		{
			_currentMouseState = Mouse.GetState();

			HandleLineInput( gameTime );

			bool lineDrawn = MakeLine();

			if ( CurrentPlayer.isComputer )
			{
				MakeLineAi();
				lineDrawn = true;
			}

			if ( lineDrawn )
			{
				int completedCells = CheckCompletedCells();

				CurrentPlayer.Score += LineScoreIncrement;
				CurrentPlayer.Score += completedCells * CellScoreIncrement;

				if ( completedCells == 0 )
				{
					_currentPlayerIndex = ( _currentPlayerIndex + 1 ) % Players.Count;
				}
			}

			if ( _currentMouseState.LeftButton == ButtonState.Released )
			{
				_lineEnd = _lineStart;
			}

			_previousMouseState = _currentMouseState;

			if ( _uncompletedCells.Count == 0 )
			{
				Players.Sort( ( p1, p2 ) =>
				{
					if ( p1.Score < p2.Score ) return 1;
					if ( p1.Score > p2.Score ) return -1;
					return 0;
				} );

				string results = String.Format( "GAME OVER! \n  \n WINNERS ARE: \n" );
				for ( int i = 0; i < Players.Count; i++ )
				{
					results = results + "\n Place " + ( i + 1 ) + " | " + Players[i].Name + " with SCORE of :" + Players[i].Score;
				}
				MessageBox.Show( results );

				IsFinished = true;
			}
		}

		private void MakeLineAi()
		{
			List<List<BiscuitCell>> lists = new List<List<BiscuitCell>>()
			{
				new List<BiscuitCell>(),
				new List<BiscuitCell>(),
				new List<BiscuitCell>(),
				new List<BiscuitCell>()
			};

			foreach ( var cell in _uncompletedCells )
			{
				var sidesCounter = cell.HasLine.Count( c => c );
				lists[sidesCounter].Add( cell );
			}
			for ( int x = 0; x < lists[3].Count; x++ )
			{
				for ( int y = 0; y < 4; y++ )
				{
					if ( !lists[3][x].HasLine[y] )
					{
						lists[3][x].HasLine[y] = true;
					}
				}
			}
			if ( lists[0].Count != 0 )
			{
				lists[0][0].HasLine[randomSides.Next( 0, 3 )] = true;
				lists[0].Remove( lists[0][0] );
			}
			//else if ( lists[1].Count != 0 )
			//{
			//	Random randomSide = new Random();

			//	lists[0][0].HasLine[randomSide.Next( 0, 4 )] = true;
			//	lists[1].Add( lists[0][0] );
			//	lists[0].Remove( lists[0][0] );
			//}
			//else if ( lists[2].Count != 0 )
			//{

			//}

		}

		private int CheckCompletedCells()
		{
			int completedCells = 0;
			for ( int x = 0; x < _uncompletedCells.Count; x++ )
			{
				var cell = _uncompletedCells[x];
				if ( cell == null ||
					cell.PlayerId != -1 )
				{
					continue;
				}

				if ( cell.HasTopLine && cell.HasBottomLine && cell.HasRightLine && cell.HasLeftLine )
				{
					_uncompletedCells.RemoveAt( x );
					cell.PlayerId = _currentPlayerIndex;
					completedCells++;
					--x;
				}

			}
			return completedCells;
		}

		public override void Draw( SpriteBatch spriteBatch, GameTime gameTime )
		{
			int playerColumnSize = 20;
			for ( int i = 0; i < Players.Count; i++, playerColumnSize += 30 )
			{
				spriteBatch.Draw( SpriteBatchExtensions.Point, new Rectangle( 0, playerColumnSize, 25, 25 ), Players[i].Color );
				string playerStatus = String.Format( " Player nr.{0}: {1} | SCORE: {2}", i + 1, Players[i].Name, Players[i].Score );

				spriteBatch.DrawString( _font, playerStatus, new Vector2( 30, playerColumnSize ), Color.Black );
			}
			spriteBatch.Draw( SpriteBatchExtensions.Point, new Rectangle( 0, playerColumnSize, 25, 25 ), Players[_currentPlayerIndex].Color );
			string currentPlayerStatus = String.Format( "Current Player Round: {0} | SCORE:{1} ", Players[_currentPlayerIndex].Name, Players[_currentPlayerIndex].Score );
			spriteBatch.DrawString( _font, currentPlayerStatus, new Vector2( 30, playerColumnSize ), Color.Black );
			DrawBiscuitBackground( spriteBatch );
			DrawBiscuit( spriteBatch );

			spriteBatch.DrawLine( _lineStart, _lineEnd, Color.Black );
		}

		private void DrawBiscuit( SpriteBatch spriteBatch )
		{
			for ( int y = 0; y < _size; y++ )
			{
				for ( int x = 0; x < _size; x++ )
				{
					if ( _mainBiscuit[y, x] == null )
					{
						continue;
					}

					BiscuitCell cell = _mainBiscuit[y, x];
					Vector2 cellPosition = new Vector2( x * CellSize, y * CellSize ) + _gameOffset;
					if ( cell.PlayerId != -1 )
					{
						Player cellPlayer = Players[cell.PlayerId];
						spriteBatch.Draw( _cellFillsTextures[cellPlayer.CellPattern], new Rectangle( ( int ) cellPosition.X, ( int ) cellPosition.Y, CellSize, CellSize ), cellPlayer.Color );
					}

					// TopLine
					if ( cell.HasTopLine )
					{
						spriteBatch.Draw( _horizontalLineTexture, new Rectangle( ( int ) cellPosition.X, ( int ) cellPosition.Y, CellSize, 3 ), Color.Black );
					}

					// BottomLine
					if ( cell.HasBottomLine )
					{
						spriteBatch.Draw( _horizontalLineTexture, new Rectangle( ( int ) cellPosition.X, ( int ) ( cellPosition.Y + CellSize ), CellSize, 3 ), Color.Black );
					}

					// LeftLine
					if ( cell.HasLeftLine )
					{
						spriteBatch.Draw( _verticalLineTexture, new Rectangle( ( int ) cellPosition.X, ( int ) cellPosition.Y, 3, CellSize ), Color.Black );
					}

					// Right Line
					if ( cell.HasRightLine )
					{
						spriteBatch.Draw( _verticalLineTexture, new Rectangle( ( int ) ( cellPosition.X + CellSize ), ( int ) cellPosition.Y, 3, CellSize ), Color.Black );
					}
				}
			}
		}

		private void DrawBiscuitBackground( SpriteBatch spriteBatch )
		{
			for ( int y = 0; y < _size; y++ )
			{
				for ( int x = 0; x < _size; x++ )
				{
					if ( _mainBiscuit[y, x] == null )
					{
						continue;
					}

					Vector2 cellPosition = new Vector2( x * CellSize, y * CellSize ) + _gameOffset;

					spriteBatch.Draw( _horizontalBackgroundLineTexture, new Rectangle( ( int ) cellPosition.X, ( int ) cellPosition.Y, CellSize, 3 ), Color.White );
					spriteBatch.Draw( _horizontalBackgroundLineTexture, new Rectangle( ( int ) cellPosition.X, ( int ) ( cellPosition.Y + CellSize ), CellSize, 3 ), Color.White );
					spriteBatch.Draw( _verticalBackgroundLineTexture, new Rectangle( ( int ) cellPosition.X, ( int ) cellPosition.Y, 3, CellSize ), Color.White );
					spriteBatch.Draw( _verticalBackgroundLineTexture, new Rectangle( ( int ) ( cellPosition.X + CellSize ), ( int ) cellPosition.Y, 3, CellSize ), Color.White );
				}
			}
		}

		private void CreateBiscuit( int size )
		{
			_size = size;
			int totalCells = ( ( _size / 2 + 1 ) * _size ) - _size / 2;
			_uncompletedCells = new List<BiscuitCell>( totalCells );
			Action<int, int> createCellFunc = ( x, y ) =>
			{
				_mainBiscuit[y, x] = new BiscuitCell { MatrixRow = y, MatrixColumn = x, PlayerId = -1 };

			};

			_mainBiscuit = new BiscuitCell[size, size];



			for ( int y = 0, xStart = size / 2, xLength = 1; y <= size / 2; y++, xStart--, xLength++ )
			{
				int xEnd = xStart + xLength;
				for ( int x = xStart; x < xEnd; x++ )
				{
					createCellFunc( x, y );
					createCellFunc( size - x - 1, y );
					createCellFunc( x, size - y - 1 );
					createCellFunc( size - x - 1, size - y - 1 );

				}

				_mainBiscuit[y, xStart].HasLeftLine = true;
				_mainBiscuit[size - y - 1, xStart].HasLeftLine = true;

				_mainBiscuit[y, size - xStart - 1].HasRightLine = true;
				_mainBiscuit[size - y - 1, size - xStart - 1].HasRightLine = true;

				_mainBiscuit[y, xStart].HasTopLine = true;
				_mainBiscuit[y, size - xStart - 1].HasTopLine = true;

				_mainBiscuit[size - y - 1, xStart].HasBottomLine = true;
				_mainBiscuit[size - y - 1, size - xStart - 1].HasBottomLine = true;
			}
			for ( int y = 0; y < size; y++ )
			{
				for ( int x = 0; x < size; x++ )
				{
					var cell = _mainBiscuit[y, x];
					if ( cell != null )
					{
						_uncompletedCells.Add( _mainBiscuit[y, x] );
					}
				}
			}

		}

		private BiscuitCell GetCellByPosition( Vector2 position )
		{
			position -= _gameOffset;
			int cellX = ( int ) position.X / CellSize;
			int cellY = ( int ) position.Y / CellSize;

			cellX = ( int ) MathHelper.Clamp( cellX, 0, _size - 1 );
			cellY = ( int ) MathHelper.Clamp( cellY, 0, _size - 1 );

			if ( _mainBiscuit[cellY, cellX] != null )
			{
				return _mainBiscuit[cellY, cellX];
			}

			return null;
		}

		private bool MakeLine()
		{
			const float floatError = 0.1f;
			if ( !( _currentMouseState.LeftButton == ButtonState.Released &&
				_previousMouseState.LeftButton == ButtonState.Pressed ) )
			{
				return false;
			}

			var initialCell = GetCellByPosition( _lineStart );
			if ( initialCell == null )
			{
				return false;
			}

			if ( _lineStart.X > _lineEnd.X + floatError ||
				_lineStart.Y > _lineEnd.Y + floatError )
			{
				Vector2 tmpEnd = _lineEnd;
				_lineEnd = _lineStart;
				_lineStart = tmpEnd;
			}

			BiscuitCell startCell = null;

			int yStart = ( int ) MathHelper.Clamp( initialCell.MatrixRow - 2, 0, _size - 1 );
			int yEnd = ( int ) MathHelper.Clamp( initialCell.MatrixRow + 2, 0, _size - 1 );

			int xStart = ( int ) MathHelper.Clamp( initialCell.MatrixColumn - 2, 0, _size - 1 );
			int xEnd = ( int ) MathHelper.Clamp( initialCell.MatrixColumn + 2, 0, _size - 1 );

			for ( int y = yStart; y <= yEnd; y++ )
			{
				for ( int x = xStart; x <= xEnd; x++ )
				{
					Vector2 leftTopCorner = new Vector2( x * CellSize, y * CellSize ) + _gameOffset;
					Vector2 startCenter = new Vector2( _lineStart.X - leftTopCorner.X, _lineStart.Y - leftTopCorner.Y );

					if ( startCenter.X * startCenter.X + startCenter.Y * startCenter.Y < CircleRadius * CircleRadius )
					{
						startCell = _mainBiscuit[y, x];

						Vector2 endCenter = new Vector2( _lineEnd.X - leftTopCorner.X, _lineEnd.Y - leftTopCorner.Y );
						if ( endCenter.X * endCenter.X + endCenter.Y * endCenter.Y < CircleRadius * CircleRadius )
						{
							return false;
						}

						break;
					}
				}
			}

			if ( startCell == null )
			{
				return false;
			}

			const float sameDimensionError = 2f;

			if ( Math.Abs( _lineStart.Y - _lineEnd.Y ) < sameDimensionError )
			{
				if ( startCell.HasTopLine )
				{
					return false;
				}

				startCell.HasTopLine = true;

				BiscuitCell topCell = _mainBiscuit[startCell.MatrixRow - 1, startCell.MatrixColumn];
				topCell.HasBottomLine = true;
				return true;
			}

			if ( Math.Abs( _lineStart.X - _lineEnd.X ) < sameDimensionError )
			{
				if ( startCell.HasLeftLine )
				{
					return false;
				}

				startCell.HasLeftLine = true;
				BiscuitCell leftCell = _mainBiscuit[startCell.MatrixRow, startCell.MatrixColumn - 1];
				leftCell.HasRightLine = true;

				return true;
			}

			return false;
		}

		private void HandleLineInput( GameTime gameTime )
		{
			if ( _currentMouseState.LeftButton == ButtonState.Pressed )
			{
				if ( _previousMouseState.LeftButton != ButtonState.Pressed )
				{
					_lineStart.X = _lineEnd.X = _currentMouseState.X;
					_lineStart.Y = _lineEnd.Y = _currentMouseState.Y;
				}

				Vector2 tmpEnd = new Vector2( _currentMouseState.X, _currentMouseState.Y );
				float length = ( tmpEnd - _lineStart ).Length();
				Vector2 virtualEnd = new Vector2( _lineStart.X + MathHelper.Clamp( length, 0, CellSize ), _lineStart.Y );

				var alpha = MathHelper.ToDegrees( ( float ) ( Math.Atan2( _lineStart.Y - tmpEnd.Y, _lineStart.X - tmpEnd.X ) + Math.PI ) );

				_lineEnd = GetNearestPoint( virtualEnd, alpha, 90.0f );
			}
		}

		private Vector2 GetNearestPoint( Vector2 initialPoint, float currentAngle, float angleIncrements )
		{
			var prevAlpha = ( ( int ) ( currentAngle / angleIncrements ) * angleIncrements );
			var nextAlpha = ( ( ( int ) ( currentAngle / angleIncrements ) ) + 1 ) * angleIncrements;

			var finalAlpha = Math.Abs( currentAngle - prevAlpha ) < Math.Abs( nextAlpha - currentAngle ) ? prevAlpha : nextAlpha;

			return Vector2.Transform( initialPoint,
				Matrix.CreateTranslation( -_lineStart.X, -_lineStart.Y, 0 ) *
				Matrix.CreateRotationZ( MathHelper.ToRadians( finalAlpha ) ) *
				Matrix.CreateTranslation( _lineStart.X, _lineStart.Y, 0 ) );
		}

	}

	class BiscuitCell
	{
		public int MatrixRow;
		public int MatrixColumn;
		public bool[] HasLine = new bool[4];

		public bool HasTopLine
		{
			get { return HasLine[0]; }
			set { HasLine[0] = value; }
		}
		public bool HasRightLine
		{
			get { return HasLine[1]; }
			set { HasLine[1] = value; }
		}
		public bool HasBottomLine
		{
			get { return HasLine[2]; }
			set { HasLine[2] = value; }
		}
		public bool HasLeftLine
		{
			get { return HasLine[3]; }
			set { HasLine[3] = value; }
		}
		public int PlayerId;
	}
}
