﻿
using Microsoft.Xna.Framework;

namespace PaperGames
{
	public class Player
	{
		public string Name { get; set; }
		public Color Color { get; set; }
		public int Score { get; set; }
		public int CellPattern { get; set; }
		public bool isComputer { get; set; }
	}

}
