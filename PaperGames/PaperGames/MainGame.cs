using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PaperGames.MiniGames;

namespace PaperGames
{
	/// <summary>
	/// This is the main type for your game
	/// </summary>
	public class MainGame : Game
	{
		GraphicsDeviceManager _graphics;
		SpriteBatch _spriteBatch;

		private Texture2D _backgroundImage;

		private BaseMiniGame _miniGame;
		private readonly MiniGameType _miniGameType;

		private List<Player> _players;
		private int _boardSize;

		public MainGame( MiniGameType miniGameType, List<Player> players, int boardSize )
		{
			_graphics = new GraphicsDeviceManager( this );

			_miniGameType = miniGameType;
			_players = players;
			_boardSize = boardSize;

			Content.RootDirectory = "Content";
			IsMouseVisible = true;
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			_spriteBatch = new SpriteBatch( GraphicsDevice );
			_backgroundImage = Content.Load<Texture2D>( "background_paper" );

			ChangeCurrentMiniGame( _miniGameType );
			SpriteBatchExtensions.Initialize( GraphicsDevice );
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update( GameTime gameTime )
		{
			base.Update( gameTime );

			if ( _miniGame != null )
			{
				_miniGame.Update( gameTime );

				if ( _miniGame.IsFinished )
				{
					Exit();
				}
			}
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw( GameTime gameTime )
		{
			GraphicsDevice.Clear( Color.CornflowerBlue );
			base.Draw( gameTime );

			_spriteBatch.Begin();
			_spriteBatch.Draw( _backgroundImage, new Rectangle( 0, 0, 1200, 1200 ), Color.White );
			if ( _miniGame != null )
			{
				_miniGame.Draw( _spriteBatch, gameTime );
			}

			_spriteBatch.End();
		}

		private void ChangeCurrentMiniGame( MiniGameType miniGameType )
		{
			BaseMiniGame newMiniGame;
			switch ( miniGameType )
			{
				case MiniGameType.Biscuit: newMiniGame = new BiscuitMiniGame( _graphics );
					break;
				default:
					throw new NotImplementedException( "No such mini game" );
			}

			newMiniGame.LoadContent( Content );
			newMiniGame.InitGame( _players, _boardSize );

			_miniGame = newMiniGame;
		}
	}
}
